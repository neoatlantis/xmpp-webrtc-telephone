all: index.html ./main.bundle.js

compress: ./main.bundle.js
	rm -f ./main.bundle.min.js
	uglifyjs ./main.bundle.js -o ./main.bundle.min.js -nc

test: index.html ./main.bundle.js
	python -m SimpleHTTPServer 8000
	xdg-open http://localhost:8000

main.bundle.js: src/*.js src/ui/*.js
	rm -f ./main.bundle.js
	browserify src/main.js -o ./main.bundle.js
