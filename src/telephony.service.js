/*
Telephony managing service

Listening:
    cmd:create-offer
    cmd:accept-offer (*)
    cmd:reject-offer (*)
    cmd:hang-up
(*)-only available when ringing

Emits:
    evt:status-changed
    evt:error
    evt:hang-up
    evt:ringing
    evt:calling
*/

var CALLER_WAIT_MAX = 60000,        // milliseconds
    RINGING_WAIT_MAX = 60000;       // milliseconds

//////////////////////////////////////////////////////////////////////////////

var postal = require('postal'),
    pubvars = require('./lib/pubvars.js');

// telephony service channel
var $telephony$ = postal.channel('telephony'),
    $internal$ = postal.channel('__internal__telphony');

// depends on following services
var $xmpp$ = postal.channel('xmpp'),
    $audio$ = postal.channel('audio');

//////////////////////////////////////////////////////////////////////////////

var RTCPeerConnection = khanRTCPeerConnection;

// -------- Telephony Session

var STATUS_IDLE = 'idle',
    STATUS_WAITING_FOR_ANSWER = 'waiting-for-answer',
    STATUS_WAITING_FOR_APPROVAL = 'waiting-for-approval',
    STATUS_CONNECTING = 'connecting';

var pc = null, buddyJID = null, _currentStatus = STATUS_IDLE;

function currentStatus(v){
    // check or declare current status
    if(undefined === v)
        // return current status
        return _currentStatus;
    else {
        // declare new current status
        _currentStatus = v; // TODO check if v is predefined constant
        $telephony$.publish('evt:status-changed', _currentStatus);
    }
}

// -------- error and hang-up handling function

function declareError(reason){
    $telephony$.publish('evt:error', reason);
    hangUp();
}

function hangUp(){ // TODO hang up reasons
    console.log('Will hang up current session.');
    // TODO cleaning up jobs, unsubscribe all things!

    $telephony$.publish('evt:hang-up', {
        jid: buddyJID,
    });
    
    // ---- Close PeerConnection
    try{
        pc.peer.close();
        pc = null;
    } catch(e) {}

    // ---- Stop audio recording
    $audio$.publish('cmd:localstream-stop');

    // ---- resume to idle status
    buddyJID = null;
    currentStatus(STATUS_IDLE);
}

$telephony$.subscribe('cmd:hang-up', hangUp);

$telephony$.subscribe('evt:hang-up', function(e){
    if(!e.jid) return;
    $xmpp$.publish('cmd:send-hang-up', e);
});
$xmpp$.subscribe('evt:receive-hang-up', function(e){
    // TODO check JID matching
    if(buddyJID && e.jid != buddyJID) return;
    hangUp();
});

// -------- Methods to prepare the whole system for a new telephony call
//
//          Call `promiseGetReady()` to get a Promise, which tests all
//          resources, like `audio` and `xmpp`, are ready within a time limit.
//          It then prepares a new PeerConnection, bind necessary event
//          handlers and adds the Audio stream on it.

function promiseGetReady(){
    // Promise #1: XMPP is ready
    var p1 = new Promise(function(resolve, reject){
        $xmpp$.subscribe('evt:ready', resolve).once();
        $xmpp$.subscribe('evt:not-ready', reject).once();
    });
    // Promise #2: Audio localstream is ready
    var p2 = new Promise(function(resolve, reject){
        $audio$.subscribe('evt:localstream-error', function(err){
            reject(err);
        }).once();
        $audio$.subscribe('evt:localstream-ready', function(){
            resolve();
        }).once();
    });

    var taskPromise = Promise.all([p1, p2]),
        timeoutPromise = new Promise(function(resolve, reject){
            setTimeout(reject, 3000);
        });

    $xmpp$.publish('cmd:ready');
    $audio$.publish('cmd:localstream-ready');

    return Promise.race([taskPromise, timeoutPromise]);
}

// -------- Methods to create an outgoing call

function _giveUpOffer(){
    if(currentStatus() != STATUS_WAITING_FOR_ANSWER) return;
    hangUp();
}

function _sendOffer(offer, toJID){
    buddyJID = toJID;
    currentStatus(STATUS_WAITING_FOR_ANSWER); // declare new status
    $xmpp$.publish('cmd:send-offer', {
        jid: buddyJID,
        offer: offer,
    });
    // automatically hang up after duration of CALLER_WAIT_MAX
    setTimeout(_giveUpOffer, CALLER_WAIT_MAX);
    $telephony$.publish('evt:calling', {
        jid: buddyJID,
        offer: offer,
    });
    console.log('Send offer.');
}

function initByCreatingOffer(toJID){
    if(currentStatus() != STATUS_IDLE) return;
    promiseGetReady().then(function(){
        console.log('*** creating PeerConn ***');
        var localStream = pubvars('audio.localstream');
        pc = RTCPeerConnection({
            attachStream: localStream,
            onICE: sendICE,
            onRemoteStream: onAddStream,
            onRemoteStreamEnded: onRemoveStream,
            onOfferSDP: function (offerSDP){
                _sendOffer(offerSDP, toJID);
            },
            onChannelMessage: onChannelMessage,
            onChannelOpened: onChannelOpened, 
        });
    }, declareError);
}

$xmpp$.subscribe('evt:receive-answer', function receiveAnswer(data){
    if(currentStatus() != STATUS_WAITING_FOR_ANSWER) return;
    var answer = data.answer, jid = data.jid;
    // TODO test JID matching
    pc.addAnswerSDP(answer);
    currentStatus(STATUS_CONNECTING); // declare new status
    console.log("Peer answer handled.");
});


// -------- Methods for handling an incoming call

var tempOfferHolder = null, tempJID = null;

function _sendAnswer(answer, toJID){
    buddyJID = toJID;
    $xmpp$.publish('cmd:send-answer', {
        jid: buddyJID,
        answer: answer,
    });
    currentStatus(STATUS_CONNECTING); // declare new status
    console.log("Answer sent to peer.");
}

function _acceptOffer(){
    if(currentStatus() != STATUS_WAITING_FOR_APPROVAL) return;
    if(null === tempOfferHolder) return;
    
    (function(offer, toJID){
        pc = RTCPeerConnection({
            attachStream: pubvars('audio.localstream'),
            onICE: sendICE,
            offerSDP: tempOfferHolder,
            onAnswerSDP: function(answerSDP){
                _sendAnswer(answerSDP, toJID);
            },
            onChannelMessage: onChannelMessage,
            onChannelOpened: onChannelOpened, 
            onRemoteStream: onAddStream,
            onRemoteStreamEnded: onRemoveStream,
        });
    })(tempOfferHolder, tempJID);

    tempOfferHolder = null;
    tempJID = null;
    console.log("Telephony:AcceptOffer");
}

function _rejectOfferOverXMPP(toJID){
    // send a rejection over XMPP
    $xmpp$.publish('cmd:send-hang-up', {jid: toJID});
}

function _rejectOffer(){
    if(currentStatus() != STATUS_WAITING_FOR_APPROVAL) return;
    _rejectOfferOverXMPP(tempJID);
    hangUp();
    tempOfferHolder = null;
    tempJID = null;
    console.log("Telephony:RejectOffer");
}

function _declareRingingOverXMPP(toJID){
    // send over XMPP telling we are ringing
    $xmpp$.publish('cmd:send-ringing', {jid: toJID})
}

function initByReceivingOffer(offer, fromJID){
    // To receive an offer...
    // #1 check if we're in IDLE mode.
    if(currentStatus() != STATUS_IDLE) return _rejectOfferOverXMPP(fromJID);
    // #2 we have to ready our system
    var promiseReady = promiseGetReady();
    // #3 publish offer to wait for approval
    promiseReady.then(function(){
        // record offer and remote jid
        tempOfferHolder = offer;
        tempJID = fromJID;
        // prepare event handlers for user interaction
        $telephony$.subscribe('cmd:accept-offer', _acceptOffer).once();
        $telephony$.subscribe('cmd:reject-offer', _rejectOffer).once();
        // ring now
        $telephony$.publish('evt:ringing', {
            offer: offer,
            jid: fromJID,
        });
        currentStatus(STATUS_WAITING_FOR_APPROVAL);
        _declareRingingOverXMPP(fromJID);
        // ring for maximal duration of RINGING_WAIT_MAX
        setTimeout(_rejectOffer, RINGING_WAIT_MAX);
    }, declareError);
}

$xmpp$.subscribe('evt:receive-offer', function(data, evt){
    initByReceivingOffer(data.offer, data.jid);
});

// -------- Methods for connection negotiating(ICE info exchange)
//          Notice: processIce() must be called after current status is
//          STATUS_CONNECTING(meaning an answer is sent or received). But such
//          ICEs can be given before an answer, so we have to collect and cache
//          them.

var _cachedICEs = [];
var _cacheRemoteICEAndWait = function(){
    if(_cachedICEs.length < 1) return;
    if(currentStatus() == STATUS_IDLE){
        // Connection end or broken. given up and purge cache.
        _cachedICEs = [];
        return;
    }
    if(currentStatus() == STATUS_CONNECTING){
        // once connected, purge cache and call `processIce`.
        _cachedICEs.forEach(function(ice){
            pc.addICE(ice);
        });
        console.log(_cachedICEs.length + " ICE candidates processed.");
        _cachedICEs = [];
    }
}
setInterval(_cacheRemoteICEAndWait, 100);

function sendICE(ice){
    if(currentStatus() == STATUS_IDLE) return;
    $xmpp$.publish('cmd:send-ice', {
        jid: buddyJID || tempJID,
        ice: ice,
    });
    console.log("ICE sent to peer.");
}

function receiveICE(ice){
    if(currentStatus() == STATUS_IDLE) return;
    console.log('ICE received from peer. Cached for later use.');
    _cachedICEs.push(ice);
}

$xmpp$.subscribe('evt:receive-ice', function(data, evt){
    receiveICE(data.ice);
});

// -------- Methods for connection status
//
//          Following functions were to be attached to PeerConnection within
//          `_initializePeerConnection` function

function onAddStream(stream){
    $audio$.publish('cmd:remotestream-add', stream);
}

function onRemoveStream(stream){
    $audio$.publish('cmd:remotestream-remove', stream);
}

function onSignalingStateChange(){
    if(!pc) return;
    var state = pc.peer.signalingState;
    console.log('--- WEBRTC SIGNALING STATE ---', state);
}

function onICEConnectionStateChange(){
    if(!pc) return;
    var state = pc.peer.iceConnectionState;
    console.log('+++ WEBRTC ICE CONNECTION STATE +++', state);
    // read authoritive state info
    if('disconnected' == state || 'failed' == state){
        hangUp();
    }
}

// -------- Methods for creating the DataChannel, test purpose

function onChannelMessage(d){
    console.log('Channel message', d);
    $telephony$.publish('evt:channel-data', d);
}

function onChannelOpened(){
    console.log('Channel opened.');
    $telephony$.publish('evt:channel-open');

    setInterval(function(){
        $telephony$.publish('cmd:channel-data', 'hello');
    }, 10000);
}

$telephony$.subscribe('cmd:channel-data', function(d){
    if(pc) pc.sendData(d);
});


//////////////////////////////////////////////////////////////////////////////

// -------- APIs

$telephony$.subscribe('cmd:create-offer', function(data, e){
    initByCreatingOffer(data.jid);
});

//////////////////////////////////////////////////////////////////////////////

module.exports = function(){}
module.exports.STATUS_IDLE = STATUS_IDLE;
module.exports.STATUS_WAITING_FOR_ANSWER = STATUS_WAITING_FOR_ANSWER;
module.exports.STATUS_WAITING_FOR_APPROVAL = STATUS_WAITING_FOR_APPROVAL;
module.exports.STATUS_CONNECTING = STATUS_CONNECTING;
