var storage = {};

module.exports = function(key, value){
    if(undefined === value){
        return storage[key];
    } else {
        storage[key] = value;
    }
}
