module.exports = function(){}

var postal = require('postal'),
    $xmpp$ = postal.channel('xmpp'),
    $internal$ = postal.channel('__internal__ui');


var $ = require('jquery');
var selector = '.window.login';

function $$(s){ return $(selector).find(s); };

//////////////////////////////////////////////////////////////////////////////

function show(){
    $(selector).show();
    $internal$.publish('evt:login-show');
}

function hide(){
    $(selector).hide();
    $internal$.publish('evt:login-hide');
}

$$('form').submit(function(){
    var jid = $('#window-login-username').val(),
        pwd = $('#window-login-password').val();
    $xmpp$.publish('cmd:login', {jid: jid, password: pwd});
    return false;
});

$xmpp$.subscribe('evt:login', function(){
    hide();
});

$xmpp$.subscribe('evt:logout', function(){
    show();
});

module.exports.show = show;
module.exports.hide = hide;

$(selector).show();
