module.exports = function(){}

var postal = require('postal'),
    $xmpp$ = postal.channel('xmpp'),
    $telephony$ = postal.channel('telephony'),
    $internal$ = postal.channel('__internal__ui');
var telephony = require('../telephony.service.js');

var $ = require('jquery');
var selector = '.window.call';

function $$(s){ return $(selector).find(s); };

//////////////////////////////////////////////////////////////////////////////

function show(config){
    $(selector).show();
    if(config.jid) $('#window-call-buddyjid').text(config.jid);

    if(config.showAcceptButton)
        $('#window-call-accept').show();
    else
        $('#window-call-accept').hide();
    if(config.showRejectButton)
        $('#window-call-reject').show();
    else
        $('#window-call-reject').hide();
    if(config.showHangupButton)
        $('#window-call-hangup').show();
    else
        $('#window-call-hangup').hide();
}

function hide(){
    $(selector).hide();
}

$$('form').submit(function(){
    var jid = $('#window-login-username').val(),
        pwd = $('#window-login-password').val();
    $xmpp$.publish('cmd:login', {jid: jid, password: pwd});
    return false;
});

$('#window-call-accept').click(function(){
    $telephony$.publish('cmd:accept-offer');
});

$('#window-call-reject').click(function(){
    $telephony$.publish('cmd:reject-offer');
});

$('#window-call-hangup').click(function(){
    $telephony$.publish('cmd:hang-up');
});


$telephony$.subscribe('evt:calling', function(data){
    // calling buddy proactively
    var jid = data.jid,
        offer = data.offer;
    show({
        jid: jid,
        showAcceptButton: false,
        showRejectButton: false,
        showHangupButton: true,
    });
});

$telephony$.subscribe('evt:ringing', function(data){
    // incoming call 
    var jid = data.jid,
        offer = data.offer;
    show({
        jid: jid,
        showAcceptButton: true,
        showRejectButton: true,
        showHangupButton: false,
    });
});

$telephony$.subscribe('evt:status-changed', function(newStatus){
    if(newStatus == telephony.STATUS_CONNECTING){
        show({
            showHangupButton: true,
            showAcceptButton: false,
            showRejectButton: false,
        });
    }
});

$telephony$.subscribe('evt:hang-up', function(){
    hide();
});

$(selector).hide();
