module.exports = function(){}

var postal = require('postal'),
    $xmpp$ = postal.channel('xmpp'),
    $telephony$ = postal.channel('telephony'),
    $internal$ = postal.channel('__internal__ui');


var $ = require('jquery');
var selector = '.window.main';

function $$(s){ return $(selector).find(s); };

//////////////////////////////////////////////////////////////////////////////

// -------- on a roster item is clicked / double clicked

function onRosterItemClicked(){
}

function onRosterItemDblclicked(){
    var jid = $(this).attr('data-jid');
    $telephony$.publish('cmd:create-offer', {jid: jid});
}

// -------- display up-to-dated roster list

function _sortRoster(){
}

function updateRoster(roster){
    var parentSelector = '#window-main-roster';
    $(parentSelector).find('.list-group-item').each(function(){
        var jid = $(this).attr('data-jid');
        if(!roster[jid]) return $(this).remove();
    });
    var item;
    for(var jid in roster){
        item = $(parentSelector).find('[data-jid="' + jid + '"]');
        if(item.length < 1){
            // create item
            item = $('<a>', {href: '#'}).addClass('list-group-item');
            item.attr('data-jid', jid);
            $('<h4>').addClass('list-group-item-heading').appendTo(item);
            $('<p>').addClass('list-group-item-text').appendTo(item);
            // item add to screen and attach events
            item.appendTo('#window-main-roster');
            item.click(onRosterItemClicked).dblclick(onRosterItemDblclicked);
        }
        item.find('.list-group-item-heading').text(roster[jid].name);
        item.find('.list-group-item-text').text(jid);
    }
    _sortRoster();
}

$xmpp$.subscribe('evt:roster', function(data, env){
    // update roster with new data
    updateRoster(data);
});

// -------- hide window when login shows, vice versa

function show(){
    $(selector).show();
    $internal$.publish('evt:main-show');
}

function hide(){
    $(selector).hide();
    $internal$.publish('evt:main-hide');
}

$internal$.subscribe('evt:login-show', hide);
$internal$.subscribe('evt:login-hide', show);

//////////////////////////////////////////////////////////////////////////////

module.exports.show = show;
module.exports.hide = hide;

$(selector).hide();
