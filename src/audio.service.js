/*
 * Service for obtaining and playing audio stream.
 *
 * Listens:
 *  cmd:localstream-ready     | * |   |   |   |
 *  cmd:localstream-stop      |   | * |   |   |
 *  cmd:remotestream-add      |   |   |   |   |
 *  cmd:remotestream-remove   |   |   |   |   |
 *
 * Emits:
 *  evt:localstream-ready     | * |   |   |   |
 *  evt:localstream-stop      |   | * |   |   |
 *  evt:localstream-error     | * |   |   |   |
 *
 * When local audio stream obtained(after emitting `evt:localstream-ready`),
 * the obtained audio stream can be accessed at `pubvars('audio.localstream')`
 */
module.exports = function(){}


var postal = require('postal'),
    $ = require('jquery'),
    getUserMedia = require('getusermedia'),
    attachMediaStream = require('attachmediastream'),
    pubvars = require('./lib/pubvars.js');

var $audio$ = postal.channel('audio');


// -------- get local user audio stream

$audio$.subscribe('cmd:localstream-ready', function(){
    var cached = pubvars('audio.localstream');
    if(cached) return $audio$.publish('evt:localstream-ready');
    getUserMedia({audio: true, video: true}, function(err, stream){
        if(err) return $audio$.publish('evt:localstream-error', err);
        pubvars('audio.localstream', stream);
        $audio$.publish('evt:localstream-ready');

        attachMediaStream(stream, $('#local-media')[0], {
            muted: true,
            autoplay: true,
        });
    });
});

$audio$.subscribe('cmd:localstream-stop', function(){
    var cached = pubvars('audio.localstream');
    if(cached){
        cached.getAudioTracks().forEach(function(e){ e.stop(); });
        cached.getVideoTracks().forEach(function(e){ e.stop(); });
        pubvars('audio.localstream', null);
    }
    $('#local-video').attr('src', '');
    $audio$.publish('evt:localstream-stop');
});

// -------- set or remove remote audio stream

$audio$.subscribe('cmd:remotestream-add', function(data, env){
    var stream = data;
    var obj = $('#remote-media')[0];
    attachMediaStream(stream, obj, {
        muted: false,
        autoplay: true,
    });
    obj.volume = 1;
    console.log('Remote stream addition.');
});

$audio$.subscribe('cmd:remotestream-remove', function(data, env){
    var audios = $('#remote-media');
    audios.each(function(){ this.pause(); });
    audios.attr('src', '');
    console.log('Remote stream removal.');
});
