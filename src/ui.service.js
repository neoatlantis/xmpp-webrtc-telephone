module.exports = function(){ }

var postal = require('postal');
var $ui$ = postal.channel('ui'),
    $internal$ = postal.channel('__internal__ui');

var windows = {
    login: require('./ui/login.js'),
    main: require('./ui/main.js'),
    call: require('./ui/call.js'),
};
