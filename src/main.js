var $ = require('jquery');
$(function(){

    var services = {
        ui: require('./ui.service.js'),
        xmpp: require('./xmpp.service.js'),
        audio: require('./audio.service.js'),
        telephony: require('./telephony.service.js'),
    };
    for(var each in services) services[each]();

});
