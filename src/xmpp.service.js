/* 
XMPP backend: Automatic Manager of Network Connection and Communications
*/

module.exports = function(){ }

require('strophe');
var postal = require('postal');
var $ = require('jquery');

var $internal$ = postal.channel('__internal__xmpp');
var $session$ = postal.channel('session'),
    $xmpp$ = postal.channel('xmpp');

IFF_WORD = "neoatlantis-telephony";

INCOMING_CALL_NOT_SUPPORTED_WORD = (
    "This is an incoming call from NeoAtlantis WebRTC-XMPP Telephone. " +
    "Since you've read this message, seems your software were unable to " +
    "handle such communications."
)

//////////////////////////////////////////////////////////////////////////////

function XMPPInstance(){
    var self = this;

    var url = 'http:';
    if('https:' == window.location.protocol) url = 'https:';
    url += '//neoatlantis.info/http-bind';
    var conn = new Strophe.Connection(url);

    var currentStatus = null;
    function onConnectionStatusChanged(stat, err){
        var s = Strophe.Status;
        currentStatus = stat;
        console.log('XMPP', stat);
        if(stat == s.CONNECTING) return onConnecting();
        if(stat == s.CONNFAIL) return onConnfail();
        if(stat == s.AUTHENTICATING) return onAuthenticating();
        if(stat == s.AUTHFAIL) return onAuthfail();
        if(stat == s.CONNECTED) return onConnected();
        if(stat == s.DISCONNECTED) return onDisconnected();
        if(stat == s.DISCONNECTING) return onDisconnecting();
        if(stat == s.ATTACHED) return onAttached();
        onError(err);
    }

    function onAuthfail(){
        doAutoReconnection = false;
    }

    function onDisconnected(){
        if(doAutoReconnection) doConnect();
        $internal$.publish('evt:logout');
    }

    function onConnecting(){}
    function onConnfail(){}
    function onAuthenticating(){}
    function onConnected(){
        $internal$.publish('evt:login');
        declarePresence();
        retrieveRoster();
    }
    function onDisconnecting(){}
    function onAttached(){}

    function onError(err){
        $internal$.publish('evt:error', err);
    }


    // -------- Connectivity Auto Management
    
    var doAutoReconnection = false, savedJID, savedPwd;

    function doConnect(jid, pwd){
        if(!jid && !pwd){
            jid = savedJID;
            pwd = savedPwd;
        } else {
            savedJID = jid;
            savedPwd = pwd;
        }
        conn.connect(jid, pwd, onConnectionStatusChanged);
    }

    this.connect = function(jid, pwd){
        doAutoReconnection = true;
        doConnect(jid, pwd);
    }

    this.disconnect = function(){
        doAutoReconnection = false;
        conn.disconnect();
    }

    // --------- Presence setting and receiving
    // Important for recognizing who else is using the same software.

    function declarePresence(){
        var pres = $pres();
        pres.c('priority').t('1').up().c('status').t(IFF_WORD);
        conn.send(pres);
    }

    // presence handler
    
    conn.addHandler(function recvPresence(s){
    }, null, 'presence');


    // -------- Roster retrieving and displaying

    var roster = {};

    // retrive roster

    function retrieveRoster(){
        var query = $iq({type: 'get'});
        query.c('query', {xmlns: 'jabber:iq:roster'});
        conn.send(query);
    }

    this.retrieveRoster = retrieveRoster;

    // roster update

    conn.addHandler(function recvRoster(s){
        var items = $(s).find('query[xmlns="jabber:iq:roster"] > item');
        items.each(function(){
            var jid = $(this).attr('jid'),
                subscription = $(this).attr('subscription'),
                name = $(this).attr('name');
            // $(this).find('group') for groups
            roster[jid] = {
                jid: jid,
                subscription: subscription,
                name: name || jid,
            };
        });
        $internal$.publish('evt:roster', roster);
    }, null, 'iq', 'result');


    // -------- offer/answer/ice sending and receiving

    function sendWebRTCInfo(prefix, jid, data){
        var str = btoa(JSON.stringify(data));
        var msg = $msg({to: jid, type: 'chat'});
        msg
            .c('body').t(INCOMING_CALL_NOT_SUPPORTED_WORD).up()
            .c('webrtc', {'type': prefix}).t(str).up()
        ;
        conn.send(msg);
//        console.log(prefix + ' sent to: ' + jid);
    }

    conn.addHandler(function recvWebRTCInfo(s){
        var stanza = $(s);
        var fromJID = stanza.attr('from');
        var entries = stanza.find('webrtc');
        var allowedPrefix = [
            'offer',
            'answer',
            'ice',
            'hang-up',
        ];
        entries.each(function(){
            try{
                var obj = JSON.parse(atob($(this).text()));
                var prefix = $(this).attr('type');
                var prefixAllowed = false;
                for(var i in allowedPrefix){
                    if(allowedPrefix[i] == prefix){
                        prefixAllowed = true;
                        break;
                    }
                }
                if(!prefixAllowed) return;
                var data = {jid: fromJID};
                data[prefix] = obj;
                $internal$.publish('evt:receive-' + prefix, data);
//                console.log(prefix + ' from ' + fromJID + ' received.');
            } catch(e){
                console.error(e);
            }
        });
        return true; // important for this handler to be always alive
    }, null, 'message', 'chat');

    this.sendOffer = function(jid, offer){
        sendWebRTCInfo('offer', jid, offer);
    }

    this.sendAnswer = function(jid, answer){
        sendWebRTCInfo('answer', jid, answer);
    }

    this.sendICE = function(jid, ice){
        sendWebRTCInfo('ice', jid, ice);
    }

    this.sendHangUp = function(jid){
        sendWebRTCInfo('hang-up', jid, '');
    }

    // --------

    this.send = function(jid, message){
        var msg = $msg({to: jid, type: 'chat', xmlns: 'jabber:client'});
        msg.c('body').t(message);
        conn.send(msg);
    }

    // -------- readiness report

    this.isReady = function(){
        return currentStatus == Strophe.Status.CONNECTED;
    }
    
    return this;
};

//////////////////////////////////////////////////////////////////////////////

var xmppInstance = new XMPPInstance();

function internal2external(e){
    $internal$.subscribe(e, function(data, env){ $xmpp$.publish(e, data); });
}

function external2internal(e){
    $xmpp$.subscribe(e, function(data, env){ $internal$.publish(e, data); });
}

// -------- handle XMPP events

internal2external('evt:login');
internal2external('evt:logout');
internal2external('evt:error');
internal2external('evt:roster');
internal2external('evt:receive-offer');
internal2external('evt:receive-answer');
internal2external('evt:receive-ice');
internal2external('evt:receive-hang-up');

// -------- process XMPP commands

$xmpp$.subscribe('cmd:login', function(data, env){
    var jid = data.jid, pwd = data.password;
    xmppInstance.connect(jid, pwd);
    console.log("XMPP account login to: " + jid);
});

$xmpp$.subscribe('cmd:logout', function(data, env){
    xmppInstance.disconnect();
});

$xmpp$.subscribe('cmd:roster', xmppInstance.retrieveRoster);

$xmpp$.subscribe('cmd:ready', function(){
    if(!xmppInstance.isReady()) return $xmpp$.publish('evt:not-ready');
    $xmpp$.publish('evt:ready');
});

$xmpp$.subscribe('cmd:send-offer', function(data){
    xmppInstance.sendOffer(data.jid, data.offer);
});

$xmpp$.subscribe('cmd:send-answer', function(data){
    xmppInstance.sendAnswer(data.jid, data.answer);
});

$xmpp$.subscribe('cmd:send-ice', function(data){
    xmppInstance.sendICE(data.jid, data.ice);
});

$xmpp$.subscribe('cmd:send-hang-up', function(data){
    console.log('send hang up');
    xmppInstance.sendHangUp(data.jid);
});

// TODO
/*
OK  evt:error

OK  cmd:login
OK  cmd:logout
OK  evt:login
OK  evt:logout

OK  cmd:roster
OK  evt:roster

OK  cmd:ready
OK  evt:ready
OK  evt:not-ready

OK  cmd:send-offer
OK  evt:receive-offer

OK  cmd:send-answer
OK  evt:receive-answer

OK  cmd:send-ice
OK  evt:receive-ice     {ice: ice, jid: fromJID}

OK  cmd:send-hang-up
OK  evt:receive-hang-up

cmd:send-ringing
cmd:send-offer-rejection

evt:receive-ringing
evt:receive-offer-rejection

*/

