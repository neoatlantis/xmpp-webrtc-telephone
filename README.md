VoIP using XMPP+WebRTC
======================

Finally I've decided to try this out in a pure-browser based application.

This program should provide telephone-alike functions. After starting up,
enter a XMPP account's credentials and login, and you're ready to receive
and make phone calls. 


Decided to do:
--------------
* Signaling over XMPP
* Managing contacts over XMPP's buddy list
* Audio chat only

Decided temporarily not to do(will do later):
---------------------------------------------
* multi-user-chat(MUC)
* serious authentication support
* XMPP in-band registeration
* P2P authentication using localStorage
* video chat support
